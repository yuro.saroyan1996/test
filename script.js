
$('.slider-partners').slick({
    slidesToShow: 6,
    slidesToScroll: 1,
    arrows: true,
    infinite: true,
    prevArrow: '<button type="button" class="custom-arrow slick-prev"><img src="./assets/images/arrow_to_left.png" alt=""></button>',
    nextArrow: '<button type="button" class="custom-arrow slick-next"><img src="./assets/images/arrow_to_right.png" alt=""></button>',
    responsive: [
        {
            breakpoint: 1045,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 1,
            }
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1,
            }
        },
        {
            breakpoint: 560,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
            }
        },
        {
            breakpoint: 420,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
            }
        },
        ]
});


$(document).ready(function () {



    let openMenu = false;
    $(".P-open-menu").click(function () {
        openMenu = true;
        if (openMenu) {
            $('body').css('overflow-y', 'hidden');
            console.log('asd')
            $(this).addClass("active");
            $(".P-close-menu").addClass("active");

            $(".P-menu-mobile").addClass("active");
        }


    });

    $(".P-close-menu").click(function () {
        openMenu = false;
        if (!openMenu) {
            $('body').css('overflow-y', 'auto');

            console.log('asd')
            $(this).removeClass("active");
            $(".P-open-menu").removeClass("active");
            $(".P-menu-mobile").removeClass("active");
        }


    });

});


